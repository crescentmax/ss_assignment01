var canvas, ctx, imgData, preImgData, x, y, sliderResize, sliderAlpha, startX, display;
var dataStack = [];
var redoStack = [];
var textStack = [];
var wordStack = [];
var textLength = [];
var attribute = {
    action:"brush",
    text: false,
    font: "Arial",
    fontSize: "18px",
    fontSizeNum: 18,
    color: "#000000",
    strokeFill: "No",
    lineWidth: 5
}

class Picker {
    constructor(target, width, height) {
        this.target = target;
        this.width = width;
        this.height = height;
        this.target.width = width;
        this.target.height = height;
        // Get context
        this.context = this.target.getContext("2d");
        // Circle
        this.pickerPoint = {x:0, y:220, width:7, height:7};

        this.listenForEvents();
    }

    draw() {
        this.build();
    }

    build() {
        let gradient = this.context.createLinearGradient(0, 0, this.width, 0);
        gradient.addColorStop(0, "rgb(255,0,0)");
        gradient.addColorStop(0.16, "rgb(255,0,255)");
        gradient.addColorStop(0.33, "rgb(0,0,255)");
        gradient.addColorStop(0.49, "rgb(0,255,255)");
        gradient.addColorStop(0.67, "rgb(0,255,0)");
        gradient.addColorStop(0.84, "rgb(255,255,0)");
        gradient.addColorStop(1, "rgb(255,0,0)");

        // Render
        this.context.fillStyle = gradient;
        this.context.fillRect(0, 0, this.width, this.height);

        // Black & White
        gradient = this.context.createLinearGradient(0,0,0,this.height);
        gradient.addColorStop(0, "rgba(255,255,255,1)");
        gradient.addColorStop(0.5, "rgba(255,255,255,0)");
        gradient.addColorStop(0.5, "rgba(0,0,0,0)");
        gradient.addColorStop(1, "rgba(0,0,0,1)");
        this.context.fillStyle = gradient;
        this.context.fillRect(0, 0, this.width, this.height);

        // Pick color diaplay
        this.context.beginPath();
        this.context.arc(this.pickerPoint.x, this.pickerPoint.y, this.pickerPoint.width, 0, Math.PI * 2);
        this.context.strokeStyle = "black";
        this.context.stroke();
        this.context.closePath();
    }

    listenForEvents() {
        let isMouseDown = false;
        const onMouseDown = (ev)=>{
            let currentX = ev.clientX - this.target.offsetLeft;
            let currentY = ev.clientY - this.target.offsetTop;

            // Is the mouse in picker point?
            if(currentY > this.pickerPoint.y && currentX > this.pickerPoint.x
                && currentY < this.pickerPoint.y + this.pickerPoint.width
                 && currentX < this.pickerPoint.x + this.pickerPoint.width) {
                     isMouseDown = true;
            } else {
                this.pickerPoint.x = currentX;
                this.pickerPoint.y = currentY;
            }
        }
        const onMouseMove = (ev)=>{
            if (isMouseDown) {
                let currentX = ev.clientX - this.target.offsetLeft;
                let currentY = ev.clientY - this.target.offsetTop;

                this.pickerPoint.x = currentX;
                this.pickerPoint.y = currentY;
            }
        }
        const onMouseUp = (ev)=>{

        }

        this.target.addEventListener("mousedown", onMouseDown);
        this.target.addEventListener("mousemove", onmousemove);
        this.target.addEventListener("mousemove", () => this.onChangeCallback(this.getPickedColor()));
        document.addEventListener("mouseup", onMouseUp);
    }

    getPickedColor() {
        let imageData = this.context.getImageData(this.pickerPoint.x,this.pickerPoint.y, 1, 1);
        return { r: imageData.data[0], g: imageData.data[1], b: imageData.data[2]};
    }

    onChange(callback) {
        this.onChangeCallback = callback;
    }
}




window.onload = init;

function init(){
    canvas = document.getElementById("canvas");
    ctx = canvas.getContext('2d');
    imgData = ctx.createImageData(canvas.width, canvas.height);
    sliderResize = document.getElementById("resize");
    sliderAlpha = document.getElementById("alpha");
    display = document.getElementById("display");
    dataStack.push(imgData);
    attribute.lineWidth = sliderResize.value;
    // Set brush shape
    ctx.lineJoin = 'round';
    ctx.lineCap = 'round';
    // Set alpha channel
    ctx.globalAlpha = 1;
    
    let picker = new Picker(document.getElementById("picker"), 250, 220);
    setInterval(()=>picker.draw(),1);
    picker.onChange((color) => {
        display.style.backgroundColor = `rgb(${color.r}, ${color.g}, ${color.b})`;
        attribute.color = display.style.backgroundColor;
    });

    function Mousemove(){
        this.pos = {};
        this.moving = false;    
        canvas.onmousedown = (ev)=>{
            this.moving = true;
            // Record position
            this.pos = {x:ev.offsetX, y:ev.offsetY}
            if(attribute.action == 'brush' || attribute.action == 'eraser') {
                [x, y] = [ev.offsetX, ev.offsetY];
            }
        }
    
        canvas.onmousemove = (ev)=>{
            if(!this.moving) {
                return;
            }
            // Shape?
            if(!attribute.text)
                this[attribute.action](ev);
        }
    
        canvas.onmouseup = (ev)=>{
            this.moving = false;
            // Get previous image data
            preImgData = imgData;
            // Get image data
            imgData = ctx.getImageData(0,0,canvas.width,canvas.height);
            // If not same, save previous data
            if (preImgData !== imgData)
                dataStack.push(preImgData);
            // When eraser end, set mode to source-over
            ctx.globalCompositeOperation = 'source-over';
        }
    }
    Mousemove.prototype.start = function(){
        // Clear canvas
        ctx.clearRect(0,0,canvas.width,canvas.height);
        // Put image data
        imgData && ctx.putImageData(imgData,0,0);
        // Set brush style
        ctx.strokeStyle = attribute.color;
        ctx.fillStyle = attribute.color;
        ctx.lineWidth = attribute.lineWidth;
        // Cursor icon
        cursor: attribute.mouseStyle;
    }
    Mousemove.prototype.brush = function(ev){
        ctx.strokeStyle = attribute.color;
        ctx.lineWidth = attribute.lineWidth;
        ctx.beginPath();
        ctx.moveTo(x, y);
        ctx.lineTo(ev.offsetX, ev.offsetY);
        ctx.stroke();
        [x, y] = [ev.offsetX, ev.offsetY];
    }
    Mousemove.prototype.eraser = function(ev){
        ctx.globalCompositeOperation = 'destination-out';
        ctx.strokeStyle = attribute.color;
        ctx.lineWidth = attribute.lineWidth;
        ctx.beginPath();
        ctx.moveTo(x, y);
        ctx.lineTo(ev.offsetX, ev.offsetY);
        ctx.stroke();
        [x, y] = [ev.offsetX, ev.offsetY];
    }
    Mousemove.prototype.line = function(ev){
        this.start();
        ctx.beginPath();
        // Style
        ctx.moveTo(this.pos.x, this.pos.y);
        ctx.lineTo(ev.offsetX, ev.offsetY);
        ctx.stroke();
    }
    Mousemove.prototype.rectangle = function(ev){
        this.start();
        ctx.beginPath();
        var {x, y} = this.pos;
        ctx.rect(x, y, ev.offsetX-x, ev.offsetY-y);
        ctx.stroke();
        if (attribute.strokeFill == "Fill")
            ctx.fill();
    }
    Mousemove.prototype.triangle = function(ev){
        this.start();
        ctx.beginPath();
        var {x, y} = this.pos;
        ctx.moveTo(x, y);
        ctx.lineTo(ev.offsetX, ev.offsetY);
        ctx.lineTo(2*x-ev.offsetX, ev.offsetY);
        ctx.closePath();
        ctx.stroke();
        if (attribute.strokeFill == "Fill")
            ctx.fill();
    }
    Mousemove.prototype.circle = function(ev){
        this.start();
        ctx.beginPath();
        var {x, y} = this.pos;
        var radius = Math.abs(ev.offsetX - x);
        ctx.arc(x, y, radius, 0, 2*Math.PI, true);
        ctx.stroke();
        if (attribute.strokeFill == "Fill")
            ctx.fill();
    }

    var p1 = new Mousemove();
    sliderResize.oninput = function(){
        attribute.lineWidth = sliderResize.value;
    }
    sliderAlpha.oninput = function(){
        ctx.globalAlpha = sliderAlpha.value/100;
    }
    // Text typing
    document.addEventListener("click", function(ev){
        if (attribute.text) {
            x = ev.offsetX;
            y = ev.offsetY;
            startX = x;
            // Reset word stack
            wordStack = [];
            // Reset
            textLength = [];
        }
    },false);
    // Cannot backspace to upper line
    document.addEventListener("keydown",function(ev){
        if (attribute.text) {
            ctx.font = attribute.fontSize.concat(' ', attribute.font);
            if (ev.key === "Enter") {
                saveState();
                y += attribute.fontSizeNum;
                // Save line length for backspace
                textLength.push(x - startX);
                // Save state
                wordStack.push(ev.key);
                // To line head
                x = startX;
            } else if (ev.key === "Backspace") {
                textUndo();
                var recentWord = wordStack.pop();
                console.log(recentWord);
                if (recentWord !== "Enter")
                    x -= ctx.measureText(recentWord).width;
                else {
                    y -= attribute.fontSizeNum;
                    console.log(textLength[textLength.length], x);
                    x += textLength.pop();
                }
            } else if (ev.key === "CapsLock" || ev.key === "Shift"){}
            else {
                saveState();
                ctx.fillText(ev.key, x , y);
                wordStack.push(ev.key);
                x += ctx.measureText(ev.key).width;
            }
        }
    }, false);

    function saveState() {
        textStack.push(ctx.getImageData(0,0,canvas.width,canvas.height));
    }

    function textUndo(){
        imgData = textStack.pop();
        ctx.putImageData(imgData,0,0);
    }

} // End of Init

function Brush(){
    attribute.action = "brush";
    attribute.text = false;
    document.body.style.cursor = "auto";
}

function Eraser(){
    attribute.action = "eraser";
    attribute.text = false;
    document.body.style.cursor = "auto";
}

function Text(){
    attribute.text = true;
    document.body.style.cursor = "text";
}

function Line(){
    attribute.action = "line";
    attribute.text = false;
    document.body.style.cursor = "crosshair";
}

function Rectangle(){
    attribute.action = "rectangle";
    attribute.text = false;
    document.body.style.cursor = "crosshair";
}

function Triangle(){
    attribute.action = "triangle";
    attribute.text = false;
    document.body.style.cursor = "crosshair";
}

function Circle(){
    attribute.action = "circle";
    attribute.text = false;
    document.body.style.cursor = "crosshair";
}

function Refresh(){
    ctx.clearRect(0,0,canvas.width,canvas.height);
    imgData = ctx.getImageData(0,0,canvas.width,canvas.height);
}

function Undo(){
    if(!dataStack.length)
        return;
    // Temporarily store the undo imgData
    redoStack.push(imgData);
    // Pop previous data
    imgData = dataStack.pop();
    ctx.putImageData(imgData,0,0);
}

function Redo(){
    if(!redoStack.length)
        return;
    // Push current data
    dataStack.push(imgData);
    // Pop redo data
    imgData = redoStack.pop();
    ctx.putImageData(imgData,0,0);
   
}

function Fill(){
    if (attribute.strokeFill == "No") {
        attribute.strokeFill = "Fill"
    }
    else{
        attribute.strokeFill = "No";
    }
}

function Download(){
    const a = document.createElement("a");
    a.href = canvas.toDataURL("image/png");
    a.download = "canvas-image.png";
    a.click();
    document.body.removeChild(a);
    // canvas.toBlob(
    //     blob => {
    //       const anchor = document.createElement('a');
    //       anchor.download = 'canvas.jpg'; // optional, but you can give the file a name
    //       anchor.href = URL.createObjectURL(blob);
      
    //       anchor.click(); 
      
    //       URL.revokeObjectURL(anchor.href); 
    //     },
    //     'image/jpeg',
    //     0.9,
    //   );
}

function FontSize(value){
    attribute.fontSize = value;
}

function Font(value){
    attribute.font = value;
}

window.addEventListener('load', function() {
    document.querySelector('input[type="file"]').addEventListener('change', function() {
        // Save state
        preImgData = ctx.getImageData(0,0,canvas.width,canvas.height);
        dataStack.push(preImgData);

        if (this.files && this.files[0]) {
            var image = new Image();
            image.onload = () => {
                ctx.drawImage(image,0,0);
            }
            image.src = URL.createObjectURL(this.files[0]); // set src to blob url
        }

        // When eraser end, set mode to source-over
        ctx.globalCompositeOperation = 'source-over';
    });
});